
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  //ehrId = "";

  // TODO: Potrebno implementirati
  var partyData;
  var podatki = [];
  switch (stPacienta) {
    case 1:
      partyData = {
        firstNames: "Bill",
        lastNames: "Gates",
        dateOfBirth: "1989-12-09",
        additionalInfo: {"ehrId": ""}
      };
      podatki = randomizirajPodatke(175, 70);
      break;
    case 2:
      partyData = {
        firstNames: "Jeff",
        lastNames: "Bezos",
        dateOfBirth: "1988-10-05",
        additionalInfo: {"ehrId": ""}
      };
      podatki = randomizirajPodatke(165, 65);
      break;
    case 3:
      partyData = {
        firstNames: "Mark",
        lastNames: "Zuckerberg",
        dateOfBirth: "1987-05-02",
        additionalInfo: {"ehrId": ""}
      };
      podatki = randomizirajPodatke(150, 55);
      break;
  }
  
  kreirajEHRzaBolnika(partyData, podatki);
  //return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

$(document).ready(function() {
  
  // Na izbiro vzorca v <select> elementu spodnje elemente zapolni z vrednostjo izbire, razdeljeno na vsakem ","
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });
  
  // Na izbiro vzorca v <select> elementu spodnje polje zapolni z EHR ID vrednostjo izbire
  $('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#prikazPodatkov").prop("class", "invisible row panel panel-default");
		$("#preberiEHRid").val($(this).val());
	});
	
});

function kreirajEHRzaBolnika(partyData, podatki) {
  
  $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (data) {
      var ehrId = data.ehrId;
      
      partyData.additionalInfo.ehrId = ehrId;
      
      $.ajax({
        url: baseUrl + "/demographics/party",
        type: 'POST',
        headers: {
          "Authorization": getAuthorization()
        },
        contentType: "application/json",
        data: JSON.stringify(partyData),
        success: function (party) {
          
          if (party.action == "CREATE") {
            dodajPodatkeObKreiranjuEHRzaBolnika(ehrId, podatki, function() {
              
              console.log("Zapis dodan.");
              
            });
            $("#kreirajSporocilo").html($("#kreirajSporocilo").html() + "<div class='row'><span class='" +
              "label label-success fade-in'>Kreiran EHR: " +
              ehrId + "</span></div>");
            //$("#preberiEHRid").val(ehrId);
            
            $("#preberiObstojeciEHR").html($("#preberiObstojeciEHR").html() + 
              "<option value='" + ehrId + "'>" + partyData.firstNames + " " + partyData.lastNames + "</option>");
          }
        },
        error: function(err) {
        	$("#kreirajSporocilo").html("<span class='label " +
            "label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
        }
      });
    }
	});
	
}

function preberiEHRodBolnika() {
	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='label label-warning " +
      "fade-in'>Vnesite zahtevan podatek!");
    $("#prikazPodatkov").prop("class", "invisible row panel panel-default");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			var party = data.party;
  			var date = party.dateOfBirth.split("-");
  			$("#preberiSporocilo").html("<span class='" +
          "fade-in'>Uporabnik: " + party.firstNames + " " +
          party.lastNames + "  |  Datum rojstva: " + date[2] + ". " + date[1] + ". " + date[0] +
          "</span>");
        $("#kreirajSporocilo").html("");
        $("#prikazPodatkov").prop("class", "visible row panel panel-default");
        
        preberiPodatkeObBranjuEHRzaBolnika(ehrId, party);
  		},
  		error: izpisiNapako
		});
	}
}

function dodajPodatkeObKreiranjuEHRzaBolnika(ehrId, podatki, callback) {
  
  for (var i = 0; i < podatki.length; i++) {
    var parametriZahteve = {
      ehrId: ehrId,
      templateId: 'Vital Signs',
      format: 'FLAT',
      committer: "Doktor Štefan Čudni"
    };
    
    var dataToSend = {
      "ctx/language": "en",
      "ctx/territory": "SI",
      "ctx/time": podatki[i].datum + "T" + podatki[i].ura + ":" + podatki[i].minuta + ":" + podatki[i].sekunda,
      "vital_signs/height_length/any_event:0/body_height_length": podatki[i].visina,
      "vital_signs/body_weight/any_event:0/body_weight": podatki[i].teza,
      
	    "vital_signs/blood_pressure/any_event/systolic": "90",
	    "vital_signs/blood_pressure/any_event/diastolic": "123",
	    "vital_signs/indirect_oximetry:0/spo2|numerator": "98",
	    "vital_signs/pulse/any_event/rate":"64"
    };
    
    $.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(dataToSend),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function () {
        callback();
      },
      error: izpisiNapako
    });
  }
}

function preberiPodatkeObBranjuEHRzaBolnika(ehrId, partyData) {
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/weight",
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (data) {
      var meritve = {};
      for (var i = 0; i < data.length; i++) {
        meritve[data[i].time] = {
          teza: data[i].weight,
          visina: 0
        };
      }
      $.ajax({
        url: baseUrl + "/view/" + ehrId + "/height",
        type: 'GET',
        headers: {
          "Authorization": getAuthorization()
        },
        success: function (data) {
          for (var j = 0; j < data.length; j++) {
              meritve[data[j].time].visina = data[j].height;
          }
          
          var casovi = [];
          var teze = [];
          var visine = [];
          for (var m in meritve) {
            casovi.push(m);
            teze.push(meritve[m].teza);
            visine.push(meritve[m].visina);
          }
          console.log(casovi);
          narisiGraf("graf1", "Teža", casovi, teze, "green");
          narisiGraf("graf2", "Višina", casovi, visine, "orange");
        },
        error: izpisiNapako
      });
    },
    error: izpisiNapako
  });
}

/*function narisiVseGrafe(meritve) {
  var casovi = [];
  var teze = [];
  var visine = [];
  for (var m in meritve) {
    casovi.push(m);
    teze.push(meritve[m].teza);
    visine.push(meritve[m].visina);
  }
  console.log(casovi);
  narisiGraf("graf1", "Teža", casovi, teze, "green");
  narisiGraf("graf2", "Višina", casovi, visine, "orange");
  $.ajax({
    url: "https://redepicness.ovh/health-multi.php?json=" + JSON.stringify(req),
    type: 'GET',
    success: function (data) {
      for (var i = 0; i < data.length; i++) {
        bmi.push(Math.round(data[i].bmi * 100) / 100);
        bmr.push(Math.round(data[i].bmr * 100) / 100);
      }
      mer.reverse();
      teze.reverse();
      visine.reverse();
      leta.reverse();
      bmi.reverse();
      bmr.reverse();
      izrisiGraf("graf-teza", "Teža", mer, teze, "red");
      izrisiGraf("graf-visina", "Višina", mer, visine, "blue");
      izrisiGraf("graf-bmi", "BMI", mer, bmi, "green");
      izrisiGraf("graf-bmr", "BMR", mer, bmr, "orange");
    },
    error: function (err) {
      document.getElementById("meritev-ime").innerText = datum;
      document.getElementById('error').innerText = "Napaka pri dostopu zunanjega API vmesnika!";
    }
  });
}*/

function narisiGraf(id, name, meritev, podatek, color) {
  var trace = {
    x: meritev,
    y: podatek,
    mode: 'lines+markers',
    name: name,
    line: { color: color }
  };

  var data = [trace];
  
  var font = {
    showline: true,
    showgrid: false,
    showticklabels: true,
    linecolor: 'rgb(204,204,204)',
    linewidth: 2,
    autotick: false,
    ticks: 'outside',
    tickcolor: 'rgb(204,204,204)',
    tickwidth: 2,
    ticklen: 5,
    tickfont: {
      family: 'Arial',
      size: 12,
      color: 'rgb(82, 82, 82)'
    }
  }

  var layout = {
    title: name + ' v relaciji s časom',
    showlegend: false,
    width: 550,
    height: 400,
    xaxis: {
      font,
      type: "text"
    },
    yaxis: {
      font,
      type: "number"
    },
    autosize: false,
  };

  Plotly.newPlot(id, data, layout);
}

function randomizirajPodatke(visina, teza) {
  var podatki = [];
  for (var i = 0; i < 10; i++) {
    var y = 2018 - Math.round((Math.random() * 15) % 15);
    var d1 = 1 + Math.floor(Math.random() * 12);
    var d2 = 1 + Math.floor(Math.random() * 12);
    d1 = d1 < 10 ? "0" + d1 : d1;
    d2 = d2 < 10 ? "0" + d2 : d2;
    var d = y + "-" + d1 + "-" + d2;
    
    var v = visina + Math.floor(Math.random() * 4);
    var t = teza + Math.floor(Math.random() * 8);
    var h = 1 + Math.floor(Math.random() * 23);
    var m = 1 + Math.floor(Math.random() * 59);
    var s = 1 + Math.floor(Math.random() * 59);
    
    h = h < 10 ? "0" + h : h;
    m = m < 10 ? "0" + m : m;
    s = s < 10 ? "0" + s : s;
    
    var duplicate = false;
    for (p in podatki) {
      if (p.datum == d)
        duplicate = true;
    }
    
    if (!duplicate)
      podatki.push({
        datum: d,
        ura: h,
        minuta: m,
        sekunda: s,
        visina: v,
        teza: t
      });
  }
  return podatki;
}

function izpisiNapako(err) {
	$("#preberiSporocilo").html("<span class='label " +
    "label-danger fade-in'>Napaka '" +
    JSON.parse(err.responseText).userMessage + "'!");
  $("#prikazPodatkov").prop("class", "invisible row panel panel-default");
}